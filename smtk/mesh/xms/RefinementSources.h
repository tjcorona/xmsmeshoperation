//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#ifndef __smtk_mesh_xms_RefinementSources_h
#define __smtk_mesh_xms_RefinementSources_h

#include "smtk/PublicPointerDefs.h"

#include "smtk/model/EntityRef.h"

#include <xmscore/stl/vector.h>
#include <unordered_map>

namespace smtk
{
namespace model
{
  class Face;
}

namespace mesh
{
namespace xms
{

//Represents a refinement/break line. For
struct RefinementLine
{
  ::xms::Pt3d start;
  ::xms::Pt3d end;
  double sizing;
};

//Represents a refinement point.
struct RefinementPoint
{
  enum { OFF=0, CENTER=1, HARDPOINT=2 } HardPointState;

  ::xms::Pt3d xyz;
  double sizing;
  int hardPointState;
};

class RefinementSources
{
public:
  //Needs to store break lines ( which can have sizing info )
  //If the break lines have sizing info the point along the line will
  //be added as refinement points with the associated sizing info of the line
  //
  // For refinement/break lines. We add the line explicitly as an interior
  // degenerate loop. If the line has a sizing larger than 0, we refine
  // the line when inserting so that it is segmented into pieces that
  // match the sizing requirement.
  //
  // For refinement points, all the points that are returned are points
  // which have a sizing larger than 0
  //
  RefinementSources();

  void addRefinementLine(const smtk::model::Face& face, const RefinementLine& line);
  void addRefinementPoint(const RefinementPoint& point);
  void addHardEdge(const smtk::model::Edge& edge);

  //Get all refinement points that need to be added. This should be
  //called for each face.
  const std::vector< RefinementLine >& lines(const smtk::model::Face& face) const;

  //Get all refinement points that need to be added. This should be
  //called for each face.
  //Note we will always return all the RefinementPoints, not the just the
  //ones inside the given face, since outside but near a face should
  //influence the refinement of that face
  //Note all the points returned by this call will have valid sizing
  //values ( larger than zero )
  const std::vector< RefinementPoint >& points(const smtk::model::Face& face) const;

  //Determine whether or not a model edge has been declared "hard"
  //by the user.
  bool isHardEdge(const smtk::model::Edge& edge) const;

private:
  struct EntityRefHasher
  {
    size_t operator()(const smtk::model::EntityRef& ref) const
      {
        return ref.entity().hash();
      }
  };

  typedef std::unordered_map< smtk::model::EntityRef,
                              std::vector<RefinementLine>,
                              EntityRefHasher > LineMapType;

  LineMapType m_linesPerFace;
  std::set<smtk::model::Edge> m_hardEdges;
  std::vector< RefinementPoint > m_points;
};

}
}
}

#endif
