//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/mesh/xms/operators/GenerateMesh.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ModelEntityItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/attribute/VoidItem.h"

#include "smtk/mesh/xms/ElementSizing.h"
#include "smtk/mesh/xms/ExportModel.h"
#include "smtk/mesh/xms/ImportXMSMesh.h"
#include "smtk/mesh/xms/RefinementSources.h"

#include "smtk/model/Edge.h"
#include "smtk/model/EntityIterator.h"
#include "smtk/model/Face.h"
#include "smtk/model/Model.h"
#include "smtk/model/Resource.h"
#include "smtk/model/Vertex.h"

#include "smtk/mesh/xms/GenerateMesh_xml.h"

#include <xmsmesh/meshing/MeMultiPolyMesher.h>
#include <xmsmesh/meshing/MeMultiPolyMesherIo.h>

namespace
{
smtk::mesh::xms::RefinementSources constructRefinementSources(
  const smtk::model::Model& model, const smtk::attribute::ResourcePtr& attributes)
{
  smtk::mesh::xms::RefinementSources refinementSources;

  smtk::model::EntityIterator it;
  it.traverse(model, smtk::model::ITERATE_CHILDREN);

  //query the attribute resource for local sizing information, we should cache
  //this query somewhere
  std::vector<smtk::attribute::AttributePtr> instanceAtts;
  attributes->findAttributes("LineRefinementSource",instanceAtts);

  for(auto i=instanceAtts.begin(); i != instanceAtts.end(); ++i)
  {
    const auto startItem = (*i)->findDouble("Line Start");
    const auto endItem = (*i)->findDouble("Line End");
    const auto sizingItem = (*i)->findDouble("Line Size");

    smtk::mesh::xms::RefinementLine line;
    line.start[0] = startItem->value(0);
    line.start[1] = startItem->value(1);
    line.start[2] = 0; //flatten all the point so skip z value

    line.end[0] = endItem->value(0);
    line.end[1] = endItem->value(1);
    line.end[2] = 0; //flatten all the point so skip z value

    line.sizing = sizingItem->value(0);

    //check to see what edges have this sizing
    for (it.begin(); !it.isAtEnd(); ++it)
    {
      if ( (*i)->isEntityAssociated(*it) )
      {
        refinementSources.addRefinementLine(*it, line);
      }
    }
  }

  //next query for global sizing info
  const auto attribute = *(attributes->findAttributes("Globals").begin());
  if(attribute)
  {
    auto edgeRefinementSources=
      attribute->findAs<smtk::attribute::GroupItem>("EdgeRefinementSources");
    if(edgeRefinementSources->isEnabled())
    {
      const std::size_t numGroupItems = edgeRefinementSources->numberOfGroups();
      for(std::size_t i=0; i < numGroupItems; ++i)
      {
        //todo: search these by name
        smtk::attribute::ModelEntityItemPtr edgeItem = smtk::dynamic_pointer_cast<
          smtk::attribute::ModelEntityItem>(edgeRefinementSources->item(i,0) );

        for (std::size_t j=0; j < edgeItem->numberOfValues(); j++)
        {
          refinementSources.addHardEdge(edgeItem->value(j).as<smtk::model::Edge>());
        }
      }
    }
    auto vtxRefinementSources=
      attribute->findAs<smtk::attribute::GroupItem>("VertexRefinementSources");
    if(vtxRefinementSources->isEnabled())
    {
      const std::size_t numGroupItems = vtxRefinementSources->numberOfGroups();
      for(std::size_t i=0; i < numGroupItems; ++i)
      {
        //todo: search these by name
        smtk::attribute::ModelEntityItemPtr vtxItem = smtk::dynamic_pointer_cast<
          smtk::attribute::ModelEntityItem>(vtxRefinementSources->item(i,0) );
        smtk::attribute::DoubleItemPtr sizeItem = smtk::dynamic_pointer_cast<
          smtk::attribute::DoubleItem>(vtxRefinementSources->item(i,1) );
        smtk::attribute::IntItemPtr isHardPointItem = smtk::dynamic_pointer_cast<
          smtk::attribute::IntItem>(vtxRefinementSources->item(i,2) );

        for (std::size_t j=0; j < vtxItem->numberOfValues(); j++)
        {
          smtk::mesh::xms::RefinementPoint point;
          smtk::model::Vertex vtx =
            vtxItem->value(j).as<smtk::model::Vertex>();
          point.xyz[0] = vtx.coordinates()[0];
          point.xyz[1] = vtx.coordinates()[1];
          point.xyz[2] = 0; //flatten all the point so skip z value
          point.sizing = sizeItem->value(0);
          point.hardPointState = isHardPointItem->value(0);

          if(point.sizing > 0 &&
             point.hardPointState >= smtk::mesh::xms::RefinementPoint::OFF &&
             point.hardPointState <= smtk::mesh::xms::RefinementPoint::HARDPOINT)
          {
            refinementSources.addRefinementPoint(point);
          }
        }
      }
    }
    auto ptRefinementSources=
      attribute->findAs<smtk::attribute::GroupItem>("PointRefinementSources");
    if(ptRefinementSources->isEnabled())
    {
      const std::size_t numPoints = ptRefinementSources->numberOfGroups();
      for(std::size_t i=0; i < numPoints; ++i)
      {
        //todo: search these by name
        smtk::attribute::DoubleItemPtr pointItem = smtk::dynamic_pointer_cast<
          smtk::attribute::DoubleItem>(ptRefinementSources->item(i,0) );
        smtk::attribute::DoubleItemPtr sizeItem = smtk::dynamic_pointer_cast<
          smtk::attribute::DoubleItem>(ptRefinementSources->item(i,1) );
        smtk::attribute::IntItemPtr isHardPointItem = smtk::dynamic_pointer_cast<
          smtk::attribute::IntItem>(ptRefinementSources->item(i,2) );

        smtk::mesh::xms::RefinementPoint point;
        point.xyz[0] = pointItem->value(0);
        point.xyz[1] = pointItem->value(1);
        point.xyz[2] = 0; //flatten all the point so skip z value
        point.sizing = sizeItem->value(0);
        point.hardPointState = isHardPointItem->value(0);

        if(point.sizing > 0 &&
           point.hardPointState >= smtk::mesh::xms::RefinementPoint::OFF &&
           point.hardPointState <= smtk::mesh::xms::RefinementPoint::HARDPOINT)
        {
          refinementSources.addRefinementPoint(point);
        }
      }
    }
  }
  return refinementSources;
}

smtk::mesh::xms::ElementSizing constructElementSizing(
  const smtk::model::Model& model, const smtk::attribute::ResourcePtr& attributes)
{
  smtk::mesh::xms::ElementSizing elementSizing;

  smtk::model::EntityIterator it;
  it.traverse(model, smtk::model::ITERATE_CHILDREN);

  //query the attribute resource for local sizing information
  std::vector<smtk::attribute::AttributePtr> instanceAtts;
  attributes->findAttributes("Local Sizing",instanceAtts);

  for(auto i=instanceAtts.begin(); i != instanceAtts.end(); ++i)
  {
    const auto iSizingItem = (*i)->findDouble("Sizing");
    if(!iSizingItem)
    {
      continue;
    }
    const double instanceSize = iSizingItem->value();

    //check to see what edges have this sizing
    for (it.begin(); !it.isAtEnd(); ++it)
    {
      if( (*i)->isEntityAssociated(*it) )
      {
        elementSizing.setSizing(*it, instanceSize);
      }
    }
  }

  //query the attribute resource for local bias rate
  attributes->findAttributes("Face Bias",instanceAtts);
  for(auto i=instanceAtts.begin(); i != instanceAtts.end(); ++i)
  {
    const auto iBiasItem = (*i)->findDouble("Bias");
    if(!iBiasItem)
    {
      continue;
    }
    const double instanceBias = iBiasItem->value();

    //check to see what edges have this sizing
    for (it.begin(); !it.isAtEnd(); ++it)
    {
      if( (*i)->isEntityAssociated(*it) )
      {
        elementSizing.setBias(*it, instanceBias);
      }
    }
  }

  //next query for global sizing info
  const auto attribute = *(attributes->findAttributes("Globals").begin());
  if(attribute)
  {
    smtk::attribute::ConstDoubleItemPtr gSizingItem=
      attribute->findAs<smtk::attribute::DoubleItem>("Sizing");

    if(gSizingItem->isEnabled())
    {
std::cout<<"setting sizing to "<<gSizingItem->value()<<std::endl;
      elementSizing.setGlobalSizing(gSizingItem->value());
    }

    smtk::attribute::ConstVoidItemPtr gReverseItem=
      attribute->findAs<smtk::attribute::VoidItem>("ReverseLoops");

    elementSizing.reverse_orientation(gReverseItem->isEnabled());
  }

  return elementSizing;
}
}

namespace smtk
{
namespace mesh
{
namespace xms
{

bool GenerateMesh::ableToOperate()
{
  return smtk::operation::Operation::ableToOperate();
}

GenerateMesh::Result GenerateMesh::operateInternal()
{
  // initialize our result
  Result result = this->createResult(smtk::operation::Operation::Outcome::FAILED);

  // access the model to be meshed
  smtk::model::Model model = this->parameters()->associatedModelEntities<model::Models>()[0];

  ExportModel exportModel(constructElementSizing(model, this->specification()),
                          constructRefinementSources(model, this->specification()));

  for (auto edge : model.cellsAs<std::set<smtk::model::Edge>>())
  {
    auto meshedEdge = exportModel.make_edge(edge);
  }

  boost::shared_ptr<::xms::MeMultiPolyMesher>  mp = ::xms::MeMultiPolyMesher::New();

  ImportXMSMesh importXMSMesh;

  smtk::mesh::Resource::Ptr meshResource = smtk::mesh::Resource::create();

  for (auto face : model.cellsAs<std::set<smtk::model::Face>>())
  {
    ::xms::MeMultiPolyMesherIo polys = exportModel.make_face(face);

    bool meshed = false;
    try
    {
      meshed = mp->MeshIt(polys);
    }
    catch(...)
    {
      smtkWarningMacro(this->log(), "Failed to mesh face " << face.name());
    }

    if (meshed)
    {
      smtk::mesh::MeshSet preexistingMeshes = meshResource->meshes();
      importXMSMesh(polys, meshResource);
      smtk::mesh::MeshSet allMeshes = meshResource->meshes();
      smtk::mesh::MeshSet newMeshes = smtk::mesh::set_difference(allMeshes, preexistingMeshes);
      meshResource->setAssociation(face, newMeshes);
    }
  }

  result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);

  {
    smtk::attribute::ResourceItem::Ptr created = result->findResource("resource");
    created->setValue(meshResource);
  }

  return result;
}

const char* GenerateMesh::xmlDescription() const
{
  return GenerateMesh_xml;
}
}
}
}
