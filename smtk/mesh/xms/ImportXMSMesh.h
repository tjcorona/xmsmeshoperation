//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef __smtk_mesh_xms_ImportXMSMesh_h
#define __smtk_mesh_xms_ImportXMSMesh_h

#include "smtk/mesh/xms/Exports.h"
#include "smtk/mesh/core/Resource.h"

#include "smtk/model/Model.h"

#include <xmsmesh/meshing/MeMultiPolyMesherIo.h>

namespace smtk
{
namespace mesh
{
namespace xms
{

/// Import a xms mesh into an smtk::mesh::resource.
class SMTKXMSMESH_EXPORT ImportXMSMesh
{
public:
  explicit ImportXMSMesh();

  bool operator()(::xms::MeMultiPolyMesherIo& input,
                  const smtk::mesh::ResourcePtr& resource) const;

private:
  ImportXMSMesh(const ImportXMSMesh&) = delete;
  ImportXMSMesh& operator=(const ImportXMSMesh&) = delete;
};

}
}
}

#endif
