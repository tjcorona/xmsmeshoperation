//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/mesh/xms/ImportXMSMesh.h"

#include "smtk/model/CellEntity.h"

namespace {

typedef enum {
  // Linear cells
  VTK_EMPTY_CELL       = 0,
  VTK_VERTEX           = 1,
  VTK_POLY_VERTEX      = 2,
  VTK_LINE             = 3,
  VTK_POLY_LINE        = 4,
  VTK_TRIANGLE         = 5,
  VTK_TRIANGLE_STRIP   = 6,
  VTK_POLYGON          = 7,
  VTK_PIXEL            = 8,
  VTK_QUAD             = 9,
  VTK_TETRA            = 10,
  VTK_VOXEL            = 11,
  VTK_HEXAHEDRON       = 12,
  VTK_WEDGE            = 13,
  VTK_PYRAMID          = 14,
  VTK_PENTAGONAL_PRISM = 15,
  VTK_HEXAGONAL_PRISM  = 16,
  VTK_NUMBER_OF_CELL_TYPES
} VTKCellType;

//----------------------------------------------------------------------------
int deduce_format_version(const ::xms::VecInt& cells)
{
  int format_type = 1; //mark as old format
  const std::size_t length = cells.size();
  const std::size_t rem = length % 3;
  if(rem != 0)
    { //we are new format
    format_type = 2;
    }

  if(format_type == 1)
    { //we are divisible by 3, but could still be new format
    const bool firstCellIsTri = ( cells[0] == VTK_TRIANGLE &&
                                  cells[1] == 3);
    const bool firstCellIsQuad = ( cells[0] == VTK_QUAD &&
                                   cells[1] == 4);

    format_type = ( firstCellIsTri || firstCellIsQuad ) ? 2 : 1;
    }

  return format_type;
}

//----------------------------------------------------------------------------
bool create_triangles(const ::xms::VecInt& cells,
                      const smtk::mesh::AllocatorPtr& allocator,
                      smtk::mesh::Handle handleOffset,
                      smtk::mesh::HandleRange& createdCellIds)
{
  //now we can do the triangles!
  smtk::mesh::Handle *startOfConnectivityArray = NULL;
  const bool allocatedCells =
    allocator->allocateCells<smtk::mesh::Triangle>( (cells.size() / 3),
                                                    createdCellIds,
                                                    startOfConnectivityArray );

  if(!allocatedCells)
    {
    return false;
    }

  smtk::mesh::Handle *currentConnLoc = startOfConnectivityArray;
  for( ::xms::VecInt::const_iterator i=cells.begin(); i!=cells.end(); ++i, ++currentConnLoc)
    {
    *currentConnLoc = handleOffset + *i;
    }
  return true;
}

struct vtkToSMTK
{
  vtkToSMTK(): numCellsToAllocate(0), connectivity(NULL) {}

  std::size_t numCellsToAllocate;
  smtk::mesh::Handle* connectivity;
};

//----------------------------------------------------------------------------
template< int vtkCType, int smtkCType >
bool allocate(const smtk::mesh::AllocatorPtr& allocator,
              std::vector<vtkToSMTK>& data,
              smtk::mesh::HandleRange& createdCellIds)
{
  if (data[vtkCType].numCellsToAllocate == 0)
    return 0;

  smtk::mesh::HandleRange cellsMade;
  const bool allocatedCells =
    allocator->allocateCells<smtkCType>( data[vtkCType].numCellsToAllocate,
                                         cellsMade,
                                         data[vtkCType].connectivity );

  createdCellIds += cellsMade;

  return allocatedCells;
}

//----------------------------------------------------------------------------
bool create_cells(const ::xms::VecInt& cells,
                  const smtk::mesh::AllocatorPtr& allocator,
                  smtk::mesh::Handle handleOffset,
                  smtk::mesh::HandleRange& createdCellIds)
{
  //pass 1 determine the number of cells for each of the following types
  // Triangles
  // Quads
  std::vector< vtkToSMTK > cellTypes;
  cellTypes.resize(VTK_NUMBER_OF_CELL_TYPES);

  for( ::xms::VecInt::const_iterator i=cells.begin(); i!=cells.end();)
    {
    const std::size_t index = static_cast<std::size_t>(*i); ++i;
    const int num_pts_in_cell = *i; ++i;
    ++(cellTypes[index].numCellsToAllocate);
    i+=num_pts_in_cell;
    }

  //now lets go through the list of cell types that smtk::mesh supports
  //and allocate those
  allocate< VTK_VERTEX, smtk::mesh::Vertex>(allocator, cellTypes, createdCellIds);
  allocate< VTK_LINE, smtk::mesh::Line >(allocator, cellTypes, createdCellIds);
  allocate< VTK_TRIANGLE, smtk::mesh::Triangle >(allocator, cellTypes, createdCellIds);
  allocate< VTK_QUAD, smtk::mesh::Quad >(allocator, cellTypes, createdCellIds);
  allocate< VTK_TETRA, smtk::mesh::Tetrahedron >(allocator, cellTypes, createdCellIds);
  allocate< VTK_PYRAMID, smtk::mesh::Pyramid  >(allocator, cellTypes, createdCellIds);
  allocate< VTK_WEDGE, smtk::mesh::Wedge  >(allocator, cellTypes, createdCellIds);
  allocate< VTK_HEXAHEDRON, smtk::mesh::Hexahedron  >(allocator, cellTypes, createdCellIds);

  //we failed to allocate any cells
  if(createdCellIds.empty())
    {
    return false;
    }

  for( ::xms::VecInt::const_iterator i=cells.begin(); i!=cells.end();)
    {
    const std::size_t index = static_cast<std::size_t>(*i); ++i;
    const int num_pts_in_cell = *i; ++i;

    vtkToSMTK& mapping = cellTypes[index];
    smtk::mesh::Handle* conn = mapping.connectivity;

    for(int j=0; j < num_pts_in_cell; ++j)
      {
      *conn = handleOffset + *i;
      ++conn; ++i;
      }

    //update the position of the connectivity pointer to point to the next
    //cell to fill of this type
    mapping.connectivity = conn;
    }

  return true;
}


} //namespace

namespace smtk
{
namespace mesh
{
namespace xms
{

ImportXMSMesh::ImportXMSMesh()
{

}

bool ImportXMSMesh::operator()(::xms::MeMultiPolyMesherIo& input,
                               const smtk::mesh::ResourcePtr& resource) const
{
  //  a. First pass, count number of different cell types
  //  b. allocate storage for all the different types
  //  c. Second pass, fill the allocated storage

  // we currently have an issue where the cell format coming is could
  // be one of 2 formats based on the version of xms to which we are linking.
  // The old format would explicitly be just raw triangle point Ids, and the
  // new one is a format that allows for support of different cell types.
  //
  // Because of this we are going to use a static function variable
  // to determine if the cell array is the new or old format. This will
  // help perf

  const ::xms::VecPt3d& pts = input.m_points;
  const ::xms::VecInt& cells = input.m_cells;

  const smtk::mesh::InterfacePtr& iface = resource->interface();
  smtk::mesh::AllocatorPtr allocator = iface->allocator();

  //now we can start adding things!
  smtk::mesh::Handle handleOffset;
  std::vector< double* > allocatedCoordMemory;
  const bool allocatedPoints = allocator->allocatePoints( pts.size(),
                                                          handleOffset,
                                                          allocatedCoordMemory );
  if(!allocatedPoints)
    {
    return false;
    }

  //populate the allocated memory
  std::size_t index = 0;
  for(auto i=pts.cbegin(); i!=pts.cend(); ++i, ++index)
    {
    const ::xms::Pt3d& point = *i;
    allocatedCoordMemory[0][index] = point[0];
    allocatedCoordMemory[1][index] = point[1];
    allocatedCoordMemory[2][index] = point[2];
    }


  static int format_type = 0; //0 not found, 1 old, 2 new format
  if(format_type == 0)
    {
    format_type = deduce_format_version(cells);
    }


  smtk::mesh::HandleRange createdCellIds;
  bool create_valid = false;
  if( format_type == 1)
    { //old format
    create_valid = create_triangles(cells,
                                    allocator,
                                    handleOffset,
                                    createdCellIds);
    }
  else
    { //new format
    create_valid = create_cells(cells,
                                allocator,
                                handleOffset,
                                createdCellIds);
    }

  if(create_valid)
    {
    //now we can create a mesh for the face
    smtk::mesh::CellSet cellsForMesh(resource, createdCellIds);
    smtk::mesh::MeshSet ms = resource->createMesh(cellsForMesh);
    }
  return create_valid;
}

}
}
}
