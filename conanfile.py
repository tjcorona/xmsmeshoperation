from conans import ConanFile, CMake
from conans.errors import ConanException


class XMSMeshOperationConan(ConanFile):
    name = "XMSMeshOperation"
    version = None
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    build_requires = "boost/1.66.0@conan/stable"
    requires = "xmscore/[>=1.0.43,<2.0.0]@aquaveo/stable", \
      "xmsinterp/[>=1.0.23,<2.0.0]@aquaveo/stable", \
      "xmsgrid/[>=1.0.13,<2.0.0]@aquaveo/stable", \
      "xmsmesh/[>=1.0.0,<99.99.99]@aquaveo/stable"
