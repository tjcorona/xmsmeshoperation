#!/bin/bash

# Side note: need to add conda-forge to the set of archives
# From root directory:
# > conda build meta.yaml -c defaults -c conda-forge  --python=3.6

conda config --add channels aquaveo johnkit
conda config --set show_channel_urls true

# Build SMTK
conan install -pr ${SRC_DIR}/dev/profile_macosx_release ${SRC_DIR}
cmake \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=$PREFIX \
  ${SRC_DIR}
cmake --build . -j "${CPU_COUNT}"
cmake --build . --target install
